"""
Use Twilio API to send sms.
Usage:
    
        import sms
        sms.send('+55610123456789', 'Mensagem de teste.')
        
"""


from twilio.rest import Client

from decouple import config

ACCOUNT_SID = config('ACCOUNT_SID')

AUTH_TOKEN = config('AUTH_TOKEN')

FROM = config('FROM')


def _get_credentials():
    '''Return credentials values'''
    return Client(ACCOUNT_SID, AUTH_TOKEN)


def send(to, body):
    '''Send sms'''
    # get credentials
    credentials = _get_credentials()

    # send sms
    credentials.messages.create(to=to, from_=FROM, body=body)
    
